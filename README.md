# <p align="center">PHP Quality Tools</p>

A set of tools used to improve your PHP code quality.

## 🛠️ Tools
- [PHP CS Fixer](https://github.com/PHP-CS-Fixer/PHP-CS-Fixer)
- [PHP_Code_Sniffer](https://github.com/squizlabs/PHP_CodeSniffer)
- [PHPStan](https://phpstan.org/)
- [PHPMD](https://phpmd.org/)

## 🛠️ Installation
```bash
composer require jules-bertrand-external/php-quality-tools --dev
```

## 🧑🏻‍💻 Usage
```bash
php vendor/bin/php-quality-tools run <paths> <-c> <t tool> <-r report path>
```

Options :

`paths` : Define a list of paths (directories or files) to scan (use a comma separated list to list multiple paths). By default, will use current working directory.

`-c` : Will treat only changed files in the defined paths.

`-t <tool>` : Tool to execute, one of  "phpcs", "php-cs-fixer", "phpmd", "phpstan". If you want to execute only a specific tool.

`-r <report path>` : Will generate reports in checkstyle format in the specified path.

## 🧐 Notes
If you want to speedup the PHPMD tool, you can create the file `pdepend.xml` at the root of your project directory with the following content :

```xml
<?xml version="1.0"?>
<symfony:container xmlns:symfony="http://symfony.com/schema/dic/services"
                   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                   xmlns="http://pdepend.org/schema/dic/pdepend"
                   xsi:schemaLocation="http://symfony.com/schema/dic/services http://symfony.com/schema/dic/services/services-1.0.xsd">
    <config>
        <cache>
            <driver>memory</driver>
        </cache>
    </config>
</symfony:container>
```
