<?php

namespace PhpQualityTools\Tools;

class PhpCodeSniffer extends ProcessTool
{
    /**
     * The return string when there is no errors.
     */
    protected const NO_ERRORS_STR = '[OK] No errors';

    protected function makeCommand(array $files): array
    {
        $config = __DIR__ . '/../../phpcs.xml';

        return [
            $this->getBinDir() . '/phpcs',
            $this->format ? '--report=' . $this->format : null,
            '--standard=' . $config,
            ...$files,
        ];
    }

    public function getOutput(): string
    {
        $output = parent::getOutput();

        // In case where there is no errors, PHP CodeSniffer output is empty
        if (0 === $this->process->getExitCode() && !$output) {
            $output = self::NO_ERRORS_STR;
        }

        return $output;
    }

    /**
     * PHP CodeSniffer exit codes are really messy.
     */
    public function isSuccessful(): bool
    {
        return true;
    }

    /**
     * PHP CodeSniffer exit codes are really messy, so we're using the output to know if it has something to say.
     */
    public function hasErrors(): bool
    {
        return self::NO_ERRORS_STR !== $this->getOutput();
    }
}
