<?php

namespace PhpQualityTools\Tools;

class PhpCsFixer extends ProcessTool
{
    protected function makeCommand(array $files): array
    {
        $config = __DIR__ . '/../../php-cs-fixer.php';

        return [
            $this->getBinDir() . '/php-cs-fixer',
            'fix',
            '--config=' . $config,
            '--format=' . ($this->format ?? 'txt'),
            '--verbose',
            '--',
            ...$files,
        ];
    }

    /**
     * PHP CS Fixer is a code-style fixer, so there will never be any errors.
     */
    public function hasErrors(): bool
    {
        return false;
    }
}
