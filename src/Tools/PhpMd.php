<?php

namespace PhpQualityTools\Tools;

class PhpMd extends ProcessTool
{
    protected function makeCommand(array $files): array
    {
        $ruleset = __DIR__ . '/../../phpmd-ruleset.xml';

        return [
            $this->getBinDir() . '/phpmd',
            implode(',', $files),
            $this->format ?: 'ansi',
            $ruleset,
        ];
    }

    /**
     * PHPMD will return 0 if there is no errors, 2 if there is errors.
     */
    public function isSuccessful(): bool
    {
        $code = $this->process->getExitCode();

        return 0 === $code || 2 === $code;
    }

    public function hasErrors(): bool
    {
        return $this->isSuccessful() && 2 === $this->process->getExitCode();
    }
}
