<?php

namespace PhpQualityTools\Tools;

use Symfony\Component\Process\Process;

abstract class ProcessTool implements Tool
{
    /**
     * @var Process Tool process
     */
    protected Process $process;

    /**
     * @var string|null Output format
     */
    protected ?string $format;

    public function __construct(?string $format = null)
    {
        $this->format = $format;
    }

    public function execute(array $files): bool
    {
        $command = $this->makeCommand($files);

        $this->process = new Process($command);
        $this->process->run();

        return $this->isSuccessful();
    }

    /**
     * Return the command to run and its arguments listed as separate entries.
     *
     * @param string[] $files File list
     *
     * @return array<string|null>
     */
    abstract protected function makeCommand(array $files): array;

    public function getOutput(): string
    {
        return trim($this->process->getOutput());
    }

    public function getErrorOutput(): string
    {
        return trim($this->process->getErrorOutput());
    }

    public function isSuccessful(): bool
    {
        return $this->process->isSuccessful();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function getBinDir(): string
    {
        global $_composer_bin_dir;

        return $_composer_bin_dir ?? __DIR__ . '/../../vendor/bin';
    }
}
