<?php

namespace PhpQualityTools\Tools;

interface Tool
{
    /**
     * Execute the tool on specific file list.
     *
     * @param string[] $files File list
     */
    public function execute(array $files): bool;

    /**
     * Returns the output of the last execution.
     */
    public function getOutput(): string;

    /**
     * Returns the error output of the last execution.
     */
    public function getErrorOutput(): string;

    /**
     * Returns if the tool have been executed properly.
     */
    public function isSuccessful(): bool;

    /**
     * Returns if the tool have found some errors, warnings, fixes in the code.
     */
    public function hasErrors(): bool;
}
