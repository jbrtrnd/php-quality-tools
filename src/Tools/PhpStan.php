<?php

namespace PhpQualityTools\Tools;

class PhpStan extends ProcessTool
{
    protected function makeCommand(array $files): array
    {
        $config = __DIR__ . '/../../phpstan.neon';

        return [
            $this->getBinDir() . '/phpstan',
            'analyse',
            '--configuration=' . $config,
            '--error-format=' . ($this->format ?? 'table'),
            ...$files,
        ];
    }

    /**
     * PHPStan will return 0 if there is no errors, 1 if there is errors.
     */
    public function isSuccessful(): bool
    {
        return 2 > $this->process->getExitCode();
    }

    public function hasErrors(): bool
    {
        return $this->isSuccessful() && 1 === $this->process->getExitCode();
    }
}
