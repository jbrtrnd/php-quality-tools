<?php

namespace PhpQualityTools\Loader;

use Symfony\Component\Process\Process;

class FileLoader
{
    /**
     * @param string $path             A comma separated string of paths
     * @param bool   $onlyChangedFiles Retrieve only changed files, searching in $path argument
     *
     * @return string[] Array containing all files and directories paths
     */
    public function getPaths(string $path, bool $onlyChangedFiles): array
    {
        $paths = explode(',', $path);

        if ($onlyChangedFiles) {
            $paths = $this->getChangedFiles($paths);
        }

        return $paths;
    }

    /**
     * @param string[] $paths Paths to retrieve changed files
     *
     * @return string[] Array containing all changed files path
     */
    protected function getChangedFiles(array $paths): array
    {
        $files = [];

        $expressions = [];
        foreach ($paths as $path) {
            $expression = $path;
            if (is_dir($path)) {
                $expression .= '/*.php';
            }

            $expressions[] = $expression;
        }

        $process = new Process(['git', 'diff-index', '--name-only', '--diff-filter=AMRC', 'HEAD', ...$expressions]);
        $process->run();

        if ($process->isSuccessful()) {
            $output = $process->getOutput();
            if ($output) {
                $files = preg_split('/\r\n|\r|\n/', trim($output));
                // Just in case preg_split trigger an error
                if (!is_array($files)) {
                    $files = [];
                }
            }
        }

        return $files;
    }
}
