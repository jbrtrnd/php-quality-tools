<?php

namespace PhpQualityTools\Command;

use PhpQualityTools\Loader\FileLoader;
use PhpQualityTools\Tools\PhpCodeSniffer;
use PhpQualityTools\Tools\PhpCsFixer;
use PhpQualityTools\Tools\PhpMd;
use PhpQualityTools\Tools\PhpStan;
use PhpQualityTools\Tools\ProcessTool;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Run extends Command
{
    /**
     * @var FileLoader File loader used to retrieve files to analyze
     */
    protected FileLoader $fileLoader;

    public function __construct()
    {
        parent::__construct();
        $this->fileLoader = new FileLoader();
    }

    /**
     * Configure the "run" command.
     */
    protected function configure(): void
    {
        $this->setName('run');
        $this->setDescription('Run the set of quality tools');

        $this->addArgument(
            'path',
            InputArgument::OPTIONAL,
            'File paths to execute quality tools (comma-separated)',
            getcwd()
        );

        $this->addOption(
            'tool',
            '-t',
            InputOption::VALUE_OPTIONAL,
            'Tool to execute, one of  "phpcs", "php-cs-fixer", "phpmd", "phpstan"',
            null
        );

        $this->addOption(
            'report',
            '-r',
            InputOption::VALUE_OPTIONAL,
            'Input a directory, command will generate checkstyle reports for each tool without output',
            null
        );

        $this->addOption(
            'only-changed-files',
            '-c',
            InputOption::VALUE_NONE,
            'Execute quality tools only on changed files'
        );
    }

    /**
     * Execute the "run" command.
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('<fg=white;options=bold;bg=green>PHP Quality Tools</fg=white;options=bold;bg=green>');

        $path = $input->getArgument('path');
        $tool = $input->getOption('tool');
        $onlyChangedFiles = $input->getOption('only-changed-files');
        $report = $input->getOption('report');

        $paths = $this->fileLoader->getPaths($path, $onlyChangedFiles);
        if (!$paths) {
            $output->writeln('<error>No files found</error>');

            return Command::FAILURE;
        }

        $tools = [
            'php-cs-fixer',
            'phpcs',
            'phpstan',
            'phpmd',
        ];
        if ($tool) {
            $tools = [$tool];
        }

        if (in_array('php-cs-fixer', $tools)) {
            $this->phpCsFixer($paths, $report, $output);
        }

        if (in_array('phpcs', $tools)) {
            $this->phpCodeSniffer($paths, $report, $output);
        }

        if (in_array('phpstan', $tools)) {
            $this->phpStan($paths, $report, $output);
        }

        if (in_array('phpmd', $tools)) {
            $this->phpMd($paths, $report, $output);
        }

        return Command::SUCCESS;
    }

    /**
     * Execute PHP CS Fixer tool.
     *
     * @param string[] $paths
     *
     * @return bool Return if PHP CS Fixer executed correctly
     */
    protected function phpCsFixer(array $paths, ?string $report, OutputInterface $output): bool
    {
        $output->writeln('');
        $output->writeln('<info>PHP CS Fixer</info>');

        // Execute tool
        $phpCsFixer = new PhpCsFixer($report ? 'checkstyle' : null);

        return $this->executeProcessTool($phpCsFixer, $paths, $report, $output);
    }

    /**
     * Execute PHPStan tool.
     *
     * @param string[] $paths
     *
     * @return bool Return if PHPStan was executed correctly
     */
    protected function phpStan(array $paths, ?string $report, OutputInterface $output): bool
    {
        $output->writeln('');
        $output->writeln('<info>PHPStan</info>');

        // Execute tool
        $phpStan = new PhpStan($report ? 'checkstyle' : null);

        return $this->executeProcessTool($phpStan, $paths, $report, $output);
    }

    /**
     * Execute PHP CodeSniffer tool.
     *
     * @param string[] $paths
     *
     * @return bool Return if CodeSniffer was executed correctly
     */
    protected function phpCodeSniffer(array $paths, ?string $report, OutputInterface $output): bool
    {
        $output->writeln('');
        $output->writeln('<info>PHP CodeSniffer</info>');

        // Execute tool
        $phpCodeSniffer = new PhpCodeSniffer($report ? 'checkstyle' : null);

        return $this->executeProcessTool($phpCodeSniffer, $paths, $report, $output);
    }

    /**
     * Execute PHPMD tool.
     *
     * @param string[] $paths
     *
     * @return bool Return if PHPMD was executed correctly
     */
    protected function phpMd(array $paths, ?string $report, OutputInterface $output): bool
    {
        $output->writeln('');
        $output->writeln('<info>PHPMD</info>');

        // Execute tool
        $phpMd = new PhpMd($report ? 'checkstyle' : null);

        return $this->executeProcessTool($phpMd, $paths, $report, $output);
    }

    /**
     * Execute any process tool.
     *
     * @param string[] $paths
     *
     * @return bool Return if the tool was executed correctly
     */
    protected function executeProcessTool(
        ProcessTool $tool,
        array $paths,
        ?string $report,
        OutputInterface $output
    ): bool {
        $isSuccessful = $tool->execute($paths);

        // Lines will be printed to console output
        $lines = [
            $tool->getOutput(),
            '<info>---> Done</info>',
        ];
        if (!$isSuccessful) {
            $lines = [
                '<error>' . $tool->getErrorOutput() . '</error>',
                '<error>---> Error</error>',
            ];
        }

        // Print all lines to console output, short way
        array_map(fn ($line) => $output->writeln($line), $lines);

        if ($report) {
            if (!is_dir($report)) {
                mkdir($report);
            }

            $file = strtolower(
                ltrim(
                    (string) preg_replace(
                        '/([A-Z])/',
                        '-$1',
                        (new \ReflectionClass($tool))->getShortName()
                    ),
                    '-'
                )
            );

            file_put_contents($report . DIRECTORY_SEPARATOR . $file . '.xml', $tool->getOutput());
        }

        return $isSuccessful;
    }
}
