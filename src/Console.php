<?php

namespace PhpQualityTools;

use PhpQualityTools\Command\Run;
use Symfony\Component\Console\Application;

/**
 * Console application.
 */
class Console extends Application
{
    public function __construct()
    {
        parent::__construct('PHP Quality Tools', '1.0.3');
        $this->registerCommands();
    }

    /**
     * Register available commands in the Application.
     */
    protected function registerCommands(): void
    {
        $this->add(new Run());
    }
}
